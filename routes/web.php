<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Warga
Route::get('/home', 'WargaController@index')->name('home');
Route::get('/find', 'WargaController@index')->name('warga/find');
Route::get('/create', 'WargaController@create')->name('warga/create');
Route::post('/save', 'WargaController@save')->name('warga/save');
Route::get('/edit{id}', 'WargaController@edit')->name('warga/edit');
Route::post('/update{id}', 'WargaController@update')->name('warga/update');
Route::delete('/delete{id}', 'WargaController@destroy')->name('warga/delete');

//pages
Route::get('/admin/pages', 'HomeController@index')->name('admin/pages');


