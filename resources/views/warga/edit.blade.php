@extends('adminlte::page')


@section('content_header')
<div class="register-box">
        <div class="register-logo">
            <a href="<?php echo e(url(config('adminlte.dashboard_url', 'home'))); ?>"></a>
        </div>
        <div class="">
            <p class="login-box-msg"><?php echo 'Edit Data Warga'; ?></p>
            <form action="{{ route('warga/update', $warga->id) }}" method="post">
                <?php echo csrf_field(); ?>

                <div class="form-group has-feedback <?php echo e($errors->has('nama') ? 'has-error' : ''); ?>">
                    <input placeholder="Nama" class="form-control" type="text" name="nama" value="{{$warga->nama}}"
                           >
                    <?php if($errors->has('nama')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group has-feedback <?php echo e($errors->has('no_warga') ? 'has-error' : ''); ?>">
                    <input type="text" class="form-control" placeholder="ID Warga" name="no_warga" value="{{$warga->no_warga}}"
                           >
                    <?php if($errors->has('no_warga')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('no_warga')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                 <div class="form-group has-feedback <?php echo e($errors->has('no_rumah') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="No Rumah" type="text" name="no_rumah" value="{{$warga->no_rumah}}"
                           >
                    <?php if($errors->has('no_rumah')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('no_rumah')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

               

                <div class="form-group has-feedback <?php echo e($errors->has('nama_ayah') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="Nama Ayah" type="text" name="nama_ayah" value="{{$warga->nama_ayah}}"
                           >
                    <?php if($errors->has('nama_ayah')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama_ayah')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group has-feedback <?php echo e($errors->has('nama_ibu') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="Nama Ibu" type="text" name="nama_ibu" value="{{$warga->nama_ibu}}"
                           >
                    <?php if($errors->has('nama_ibu')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama_ibu')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group">
                      Tanggal Lahir :
                      <input  type="date" name="ttl">
                </div>

                <div class="form-group has-feedback  <?php echo e($errors->has('gender') ? 'has-error' : ''); ?>" >
                    <input type="radio" name="gender" value="1"> Laki-laki
                    <input type="radio" name="gender" value="2"> Perempuan

                    <?php if($errors->has('nama_ibu')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama_ibu')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group">
                    RT :
                    <select name="no_rt" value="{{$warga->no_rumah}}">
                      <option value="1">01</option>
                      <option value="2">02</option>
                      <option value="3">03</option>
                      <option value="4">04</option>
                    </select>
                </div>
                
                <button type="submit"
                        class="btn btn-primary btn-block btn-flat"
                ><?php echo 'Save' ; ?></button>
            </form>
        </div>
@stop

@section('content')

    
        <!-- /.form-box -->
@stop

