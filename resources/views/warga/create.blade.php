@extends('adminlte::page')


@section('content_header')
@stop

@section('content')

    <div class="register-box">
        <div class="">
            <p class="login-box-msg"><strong>Tambah Data Warga</strong></p>
            <form action="{{ route('warga/save') }}" method="post">
                <?php echo csrf_field(); ?>

                <div class="form-group has-feedback <?php echo e($errors->has('nama') ? 'has-error' : ''); ?>">
                    <input placeholder="Nama" class="form-control" type="text" name="nama" value="<?php echo e(old('nama')); ?>"
                           >
                    <?php if($errors->has('nama')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group has-feedback <?php echo e($errors->has('no_warga') ? 'has-error' : ''); ?>">
                    <input type="text" class="form-control" placeholder="ID Warga" name="no_warga" value="<?php echo e(old('no_warga')); ?>"
                           >
                    <?php if($errors->has('no_warga')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('no_warga')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                 <div class="form-group has-feedback <?php echo e($errors->has('no_rumah') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="No Rumah" type="text" name="no_rumah" value="<?php echo e(old('no_rumah')); ?>"
                           >
                    <?php if($errors->has('no_rumah')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('no_rumah')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

               

                <div class="form-group has-feedback <?php echo e($errors->has('nama_ayah') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="Nama Ayah" type="text" name="nama_ayah" value="<?php echo e(old('nama_ayah')); ?>"
                           >
                    <?php if($errors->has('nama_ayah')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama_ayah')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group has-feedback <?php echo e($errors->has('nama_ibu') ? 'has-error' : ''); ?>">
                    <input class="form-control" placeholder="Nama Ibu" type="text" name="nama_ibu" value="<?php echo e(old('nama_ibu')); ?>"
                           >
                    <?php if($errors->has('nama_ibu')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('nama_ibu')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group">
                      Tanggal Lahir :
                      <input  type="date" name="ttl">
                </div>

                <div class="form-group ">
                    <input type="radio" name="gender" value="1"> Laki-laki
                    <input type="radio" name="gender" value="2"> Perempuan
                </div>

                <div class="form-group">
                    RT :
                    <select name="no_rt">
                      <option value="1">01</option>
                      <option value="2">02</option>
                      <option value="3">03</option>
                      <option value="4">04</option>
                    </select>
                </div>

                <button type="submit"
                        class="btn btn-primary btn-block btn-flat"
                ><?php echo 'Save' ; ?></button>
            </form>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop
