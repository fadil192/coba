<!DOCTYPE html>
<html>
<head>
<style>
    .search{
        margin-left: 812px;
        padding-bottom: 3px;
    }
    .paginate{
        margin-left: 440px;
    }
</style>
    <title></title>
</head>
<body>

</body>
</html>

@extends('adminlte::page')


@section('content_header')
    <h1>Data Warga</h1>
@stop
@section('content')
               <div class="header">
                   <div class="links">
                    <a href="{{ route('warga/create')}}" class="btn btn-primary">Tambah Data Warga</a>
                </div>

                <div class="search">
                    <form action="{{ url()->current() }}">
                        <input type="text" name="keyword"  placeholder="Search">
                        <button type="submit" class="btn btn-primary">Cari</button>
                    </form>
                </div>
               </div>
                
                        <div class="table-desa">
                            
                     <table border="1px;" class="table table-striped table-hover table-condensed">
                      <thead>
                        <tr>
                          <td><strong>No</strong></td>
                          <td><strong>Warga ID</strong></td>
                          <td><strong>Nama</strong></td>
                          <td><strong>No Rumah</strong></td>
                          <td><strong>Tanggal Lahir</strong></td>
                          <td><strong>Jenis Kelamin</strong></td>
                          <td><strong>No Rukun Tetangga</strong></td>
                          <td><strong>Nama Ayah</strong></td>
                          <td><strong>Nama Ibu</strong></td>
                          <td><strong>Actions</strong></td>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($warga as $data)
                        <tr>
                          <td class="nom-list">{{$start_page}}</td>
                          <td><?php echo $data->no_warga; ?></td>
                          <td><?php echo $data->nama; ?></td>
                          <td><?php echo $data->no_rumah; ?></td>
                          <td><?php echo $data->ttl; ?></td>
                          @if($data->gender == 1)
                          <td>laki-laki </td>
                           @else 
                            <td>perempuan</td>
                          @endif
                          <td><?php echo $data->rt->rt_name; ?></td>
                          <td><?php echo $data->nama_ayah; ?></td>
                          <td><?php echo $data->nama_ibu; ?></td>
                          <td>
                            <form action="{{ route('warga/delete', $data->id)}}" method="post">
                             @method('DELETE')
                             @csrf
                             <input class="btn btn-danger" type="submit" value="Delete" />
                             
                              <a href="/edit{{ $data->id }}" class="btn btn-warning">Edit</a>
                          </td>
                            </form>
                            </tr>
                            <?php $start_page = $start_page+1 ?>
                         @endforeach
                      </tbody>
                    </table>
                        </div>

                      <div class="paginate">
                            {{ $warga->links() }}
                      </div>
                        
@stop

