<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Warga;
use App\Rt;
use DB;

class WargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $warga = Warga::when($request->keyword, function ($query) use ($request) {
        $query->where('nama', 'like', "%{$request->keyword}%")
              ->orwhere('no_warga', 'like', "%{$request->keyword}%")
              ->orWhere('nama_ayah', 'like', "%{$request->keyword}%")
              ->orWhere('nama_ibu', 'like', "%{$request->keyword}%")
              ->orWhere('no_rumah', 'like', "%{$request->keyword}%")
              ->orWhere('ttl', 'like', "%{$request->keyword}%")
              ->orWhere('no_rt', 'like', "%{$request->keyword}%");
    })->paginate(8);
        $warga->appends($request->only('keyword'));
        $start_page= (($warga->currentPage()-1) * 8) + 1;
        //$warga = Warga::with('rt')->get();

    return view('home', compact('warga'), array('start_page' => $start_page));


    }

    public function create()
    {
    	return view('warga/create');
    }

    public function save(Request $request)
    {
        $validator = $request->validate([
            'nama'      =>  'required|max:20',
            'no_warga'      =>  'required|unique:warga|max:6',
            'nama_ayah'      =>  'required|max:20',
            'nama_ibu'      =>  'required|max:20',
            'no_rumah'      =>  'required|max:5',
            'ttl'      =>  'required',
            'gender'      =>  'required',
            'no_rt'      =>  'required',
        ]);

        $form_data = array(
            'nama'      =>  $request->nama,
            'no_warga'      =>  $request->no_warga,
            'nama_ayah'      =>  $request->nama_ayah,
            'nama_ibu'      =>  $request->nama_ibu,
            'no_rumah'      =>  $request->no_rumah,
            'ttl'      =>  $request->ttl,
            'gender'      =>  $request->gender,
            'no_rt'      =>  $request->no_rt,
        );

       

        Warga::create($form_data);
        //dd('form_data');

        return redirect('home');
    }

    public function edit($id)
    {
         $warga = Warga::find($id);

        return view('warga/edit', compact('warga'));     
    }

    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'nama'      =>  'required|max:20',
            'nama_ayah'      =>  'required|max:20',
            'nama_ibu'      =>  'required|max:20',
            'no_rumah'      =>  'required|max:5',
            'no_rt'      =>  'required',
            'no_warga'      =>  'required|max:6',
            'ttl'      =>  'required',

            ]);

        $form_data = array(
            'nama'      =>  $request->nama,
            'nama_ayah'      =>  $request->nama_ayah,
            'nama_ibu'      =>  $request->nama_ibu,
            'no_rumah'      =>  $request->no_rumah,
            'no_rt'      =>  $request->no_rt,
            'no_warga'      =>  $request->no_warga,
            'ttl'      =>  $request->ttl,
            
        );  
        Warga::whereId($id)->update($form_data);

        return redirect('home');
    }

    public function destroy($id)
    {
        $warga = Warga::findOrFail($id);
        //dd($id);
        $warga->delete();

        return redirect('home');
    }

}
