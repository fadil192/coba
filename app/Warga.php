<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
    public $table = "warga";

    protected $fillable = [
        'nama', 'nama_ayah', 'nama_ibu', 'no_rumah', 'ttl', 'gender', 'no_rt', 'no_warga',
    ];

    public function rt()
    {
        return $this->belongsTo('App\Rt','no_rt','id');
    }
}
