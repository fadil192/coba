<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rt extends Model
{
    public $table = "rt";

    public function warga()
    {
        return $this->hasMany('App\Warga','no_rt','id');
    }
}
